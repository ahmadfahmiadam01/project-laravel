@extends('layout.master')

@section('judul')
Halaman Detail Film {{$film->judul}}
@endsection
    
@section('content')


<h1>{{$film->judul}} ({{$film->tahun}})</h1><br>
<img src="{{asset('gambar/'. $film->poster)}}" alt="">
<p><br>{{$film->ringkasan}}</p>

<h1>Peran</h1>


@auth
<form action="/film" method="POST" enctype="multipart/form-data" class="my-3">
    @csrf

    <div class="form-group">
        <label>Nama</label><br>
        <input type="text" name="nama" class="form">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endauth
<h1>Kritik</h1>

@foreach ($film->kritik as $item)
    <div class="card">
        <div class="card-body">
          <small><b>{{$item->users->name}}</b></small>
          <h5>{{$item->point}}</h5>
          <p class="card-text">{{$item->content}}</p>
        </div>
    </div>
@endforeach

<form action="/kritik" method="POST" enctype="multipart/form-data" class="my-3">
    @csrf

    <div class="form-group">
        <label>Content</label>
        <input type="hidden" name="film_id" value="{{$film->id}}" id=""> 
        <textarea name="content" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Point</label><br>
        <input type="number" name="point" class="form">
    </div>
    @error('point')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<a href="/film" class="btn btn-secondary">Kembali</a>


@endsection