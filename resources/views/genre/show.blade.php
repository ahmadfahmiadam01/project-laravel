@extends('layout.master')

@section('judul')
Halaman Detail Genre
@endsection
    
@section('content')

<h3>{{$genre->nama}}</h3>

<div class="row">
    @foreach ($genre->film as $item)
        <div class="col-4">
            <div class="card">
                <img src="{{asset('gambar/'. $item->poster)}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h3>{{$item->judul}}</h3>
                    <p class="card-text"> {{($item->ringkasan)}} </p>
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection