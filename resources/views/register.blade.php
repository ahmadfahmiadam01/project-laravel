@extends('layout.master')

@section('judul')
Halaman Form
@endsection
    
@section('content')
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name :</label> <br> <br>
        <input type="text" name="fname"> <br> <br>
        <label>Last Name :</label> <br> <br>
        <input type="text" name="lname"> <br> <br>
        <label>Gender</label> <br> <br>
        <input type="radio" name="wn">Male <br>
        <input type="radio" name="wn">Female <br> <br>
        <label>Nationality</label> <br> <br>
        <select name="bahasa" id=""> 
            <option value="1">Indonesia</option>
            <option value="1">Malaysia</option>
            <option value="1">Singapore</option>
        </select> <br> <br>
        <label>Language Spoken</label> <br> <br>
        <input type="checkbox" name="skill">Bahasa Indonesia <br>
        <input type="checkbox" name="skill">English <br>
        <input type="checkbox" name="skill">Other <br> <br>
        <label>Bio</label> <br> <br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
@endsection