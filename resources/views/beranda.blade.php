@extends('layout.master')

@section('judul')
Halaman Home
@endsection
    
@section('content')
<h1>Media Online</h1>
    <h3>Social Media Developer</h3>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    <h4>Benefit Join di Media Online</h4>

    <ul>
        <li>Mendapatkan motivasi dari sesama para Developer</li>
        <li>Sharing Knowledge</li>
        <li>Dibuat oleh calon web Developer terbaik</li>
    </ul>   

    <h4>Cara Bergabung ke Media Online</h4 >

    <ol>
        <li>Mengunjungi Website Ini</li>
        <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
        <li>Selesai</li>
@endsection