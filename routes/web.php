<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home');

Route::get('/data-tables', 'IndexController@datatables');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@welcome');


Route::group(['middleware' => ['auth']], function () {});

Auth::routes();

// CRUD GENRE
Route::get('/genre/create', 'GenreController@create');
Route::post('/genre', 'GenreController@store');
Route::get('/genre', 'GenreController@index');
Route::get('/genre/{genre_id}', 'GenreController@show');
Route::get('/genre/{genre_id}/edit', 'GenreController@edit');
Route::put('/genre/{genre_id}', 'GenreController@update');
Route::delete('/genre/{genre_id}', 'GenreController@destroy');

// CRUD FILM
Route::resource('film', 'FilmController');

// Route::get('/film/create', 'FilmController@create');
// Route::post('/film', 'FilmController@store');
// Route::get('/film', 'FilmController@index');
// Route::get('/film/{film_id}', 'FilmController@show');
// Route::get('/film/{film_id}/edit', 'FilmController@edit');
// Route::put('/film/{film_id}', 'FilmController@update');
// Route::delete('/film/{film_id}', 'FilmController@destroy');

Route::group(['middleware' => ['auth']], function () {
    
    // CRUD CAST
    // CREATE
    Route::get('/cast/create', 'CastController@create'); //Route menuju form create
    Route::post('/cast', 'CastController@store'); //Route untuk menyimpan data ke database
    
    //READ
    Route::get('/cast', 'CastController@index'); //Route list cast
    Route::get('/cast/{cast_id}', 'CastController@show'); //Route detail cast
    
    //UPDATE
    Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //Route menuju edit
    Route::put('/cast/{cast_id}', 'CastController@update'); //Route untuk update data berdasarkan id di database 
    
    //DELETE
    Route::delete('/cast/{cast_id}', 'CastController@destroy'); //Route untuk menghapus data dari database

    //Update Profile
    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);

    Route::resource('kritik', 'KritikController')->only([
        'index', 'store' 
    ]);

    Route::resource('peran', 'PeranController')->only([
        'index', 'store'
    ]);


});




