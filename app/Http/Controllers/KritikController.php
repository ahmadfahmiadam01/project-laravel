<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Kritik;

class KritikController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
            'point' => 'required'
        ]);

        $kritik = new Kritik;

        $kritik->content = $request->content;
        $kritik->point = $request->point;
        $kritik->users_id = Auth::id();
        $kritik->film_id = $request->film_id;

        $kritik->save();

        return redirect()->back();
    }
}
