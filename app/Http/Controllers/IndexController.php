<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function home () {
        return view('beranda');
    }

    public function layout () {
        return view('layout.master');
    }

    public function datatables () {
        return view('datatables');
    }  

}
